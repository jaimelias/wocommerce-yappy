��          �               	        '     ;     G     Y     h  0   u  3   �  �   �     v  2   }     �     �     �  	   �  �  �  
   �     �     �     �     �     �  &   �  9   !  �   [     �  /   �     -     5     P  	   k   %s number Awaiting %s payment Description Enable %s Gateway Enable/Disable Instructions Let your customers pay using %s (Banco General). Let your customers pay using Yappy (Banco General). Make your payment with %s and send us the payment receipt (screenshot). Your order will not be processed until the amount has been received in our account. Number Send your payment using %s to the number ####-#### Title Woocommerce Yappy Gateway https://www.jaimelias.com/ jaimelias Project-Id-Version: Woocommerce Yappy Gateway
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-05-11 03:38+0000
PO-Revision-Date: 2020-05-11 03:48+0000
Last-Translator: 
Language-Team: Español
Language: es_ES
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.3.3; wp-5.4.1 Número %s Esperando pago por %s Descripción Habilitar pagos por %s Habilitar/Deshabilitar Instrucciones Permite que tus clientes paguen por %s Permite que tus clientes pague por Yappy (Banco General). Realice su pago con %s y envíenos el comprobante (captura de pantalla). Su pedido no se procesará hasta que se haya recibido el pago en nuestra cuenta. Número Envíe su pago usando %s al número #### - #### Título Pagos Yappy en Woocommerce https://www.jaimelias.com/ jaimelias 